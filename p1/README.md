> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Kody Horvath

### Project 1 Requirements:

*Six Parts:*

    1. Create an application using my photo, contact inofrmation, and interests.
    2. Create a launcher icon 
    3. Add background color
    4. Add border around image and button
    5. Add text shadow
    6. Chapter questions (Ch 7, 8)

#### README.md file should include the following items:

*Screenshot of first user interface;
*Screenshot of second user interface;



#### Project 1 Screenshots:

*Screenshot of first user interface*:

![Main Page Screenshot](img/first.png)

*Screenshot of second user interface*:

![Failed Validation Screenshot](img/second.png)
