<!DOCTYPE html>
<html lang="en">
<head>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Modification of description.">
    <meta name="author" content="Kody Horvath">
    <link rel="icon" href="../img/favicon.ico">

    <title>LIS4381 - Simple Calculator</title>
        <?php include_once("../css/include_css.php"); ?>
</head>

<body>

    <?php include_once("../global/nav.php"); ?>
    <div class="container">
        <div class="starter-template">
            <div class="page-header">
                <?php include_once("global/header.php"); ?> <!--required-->

                <?php
                $num1 = $_POST['num1'];
                $num2 = $_POST['num2'];
                $operation = $_POST['operation'];

                #Display Operation
                echo "<h2>$operation</h2>";

                /*if ($operation == 'addition') {
                    $total = $num1 + $num2;
                    echo "$num1 + $num2 = $total";
                } elseif ($operation == 'subtraction') {
                    $total = $num1 - $num2;
                    echo "$num1 - $num2 = $total";
                } elseif ($operation == 'multiplication') {
                    $total = $num1 * $num2;
                    echo "$num1 x $num2 = $total";
                } elseif ($operation == 'division') {
                    $total = $num1 / $num2;
                    echo "$num1 / $num2 = $total";
                } elseif ($operation == 'exponents') {
                    $total = $num1 ** $num2;
                    echo "$num1 raised by $num2 = $total";
                }
                 */
                switch ($operation) {
                    case "Addition":
                        $total = $num1 + $num2;
                        echo "$num1 + $num2 = $total";
                        break;
                    case "Subtraction":
                        $total = $num1 - $num2;
                        echo "$num1 - $num2 = $total";
                        break;
                    case "Multiplication":
                        $total = $num1 * $num2;
                        echo "$num1 x $num2 = $total";
                        break;
                    case "Division":
                        if ($num2 != 0){
                            $total = $num1 / $num2;
                            echo "$num1 / $num2 = $total";
                        }
                        else 
                        echo "Cannot divide by zero";
                        break;
                    case "Exponent":
                        $total = $num1 ** $num2;
                        echo "$num1 raised by $num2 = $total";
                        break;
                }

                ?>
                <div>
                    <br>
                    <form action="index.php" method="post">
                    <button type="submit" class="btn btn-primary" name="return" value="return">Return</button>
                    <br>
                    <br>
                </div>
                <?php include_once("global/footer.php"); ?> <!--required-->
            </div>
        </div> <!-- end starter-template -->
 </div> <!-- end container -->
</body>
 