<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates the skills I've learned over the semester. This portoflio focuses on mobile web app development using native and non-native development.">
		<meta name="author" content="Kody Horvath">
    <link rel="icon" href="img/favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description: Create a mobile recipe app using Android Studio, create README.md file, chapter questions (Ch 3, 4)</strong>.
				</p>

				<h4>First User Interface</h4>
				<img src="img/main.png" class="img-responsive center-block" alt="Main Interface">

				<h4>Second User Interface</h4>
				<img src="img/recipe.png" class="img-responsive center-block" alt="Second Interface">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
