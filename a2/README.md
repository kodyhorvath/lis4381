# LIS 4381 - Mobile Web Application Development

## Kody Horvath

### Assignment 2 Requirements:

*Three Parts:*

    1. Create a mobile recipe app using Android Studio
    2. Create README.md file
    3. Chapter questions (Ch 3, 4)

#### README.md file should include the following items:

*Course title, your name, assignment requirements, as per A1;
*Screenshot of running application’s first user interface;
*Screenshot of running application’s second user interface;

#### Assignment Screenshots:

*Screenshot of first user interface:

![First User Interface Screenshot](img/main.png)

*Screenshot of second user interface:

![Second User Interface Screenshot](img/recipe.png)

