# LIS 4381 - Mobile Web Application Development

## Kody Horvath

### Assignment 3 Requirements:

*Four Parts:*

    1. Create a petstore database 
    2. Create a concert ticket app using Android Studio
    3. Create README.md file
    4. Chapter questions (Ch 3, 4)

#### README.md file should include the following items:

*Course title, your name, assignment requirements, as per A1;
*Screenshot of ERD;
*Screenshot of running application’s first user interface;
*Screenshot of running application’s second user interface;
*Links to the following files (a3.mwb & a3.sql)

#### Assignment Screenshots:

*Screenshot of ERD:

![First User Interface Screenshot](img/erd.png)

*Screenshot of first user interface:

![Second User Interface Screenshot](img/first.png)

*Screenshot of second user interface:

![Second User Interface Screenshot](img/second.png)

*Link to a3.sql

![a3.sql]()