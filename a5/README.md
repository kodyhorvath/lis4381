> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Kody Horvath

### Assignment 5 Requirements:

*Four Parts:*
    1. Developed a server side form data vaildation with PHP.
    2. Tested with data from MySQL ERDs and business rules (from assignment 3).
    3. Used regexp to only allow appropriate characters for each control.
    4. Chapter questions (Ch 11,12, 19)

#### README.md file should include the following items:

*Screenshot of index.php;
*Screenshot of add_petstore_process.php;
*Screenshot of add_petstore_process.php error;




#### Assignment Screenshots:

*Screenshot of index.php*:

![Main Page Screenshot](img/index.png)

*Screenshot of add_petstore_process.php*:

![Failed Validation Screenshot](img/passed.png)

*Screenshot of add_petstore_process.php error*:

![Passed Validation Screenshot](img/error.png)

*Link to local lis4381 web app *
[A5 Link to local repo](http://localhost/repos/lis4381/ "My Local Repo")