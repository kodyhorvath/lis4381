# LIS 4381 - Mobile Web Application Development

## Kody Horvath

### Main README file

*Course Work Links (Relative link example):*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS 
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete bitbucket tutorials
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create a mobile recipe app using Android Studio

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create a database for a petstore owner
    - USe SQL statements to show tables
    - Create a mobile application for an event (ex. concert tickets)

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a business card using previous assingments

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Clone starter files
    - Review subdirectories and files
    - Review index.php code
    - Research validation code
    - Create a favicon
    - Chapter questions (Ch 9, 10, 15)

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Developed a server side form data vaildation with PHP.
    - Tested with data from MySQL ERDs and business rules (from assignment 3).
    - Used regexp to only allow appropriate characters for each control.
    - Chapter questions (Ch 11,12, 19)

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Turn off client-side validation
    - Add server side validation
    - Used regexp to only allow appropriate characters for each control.
    - Create two PHP pages (edit_petstore & edit_petstore_process)
    - Allow edit & delete of records
    - Chapter questions (Ch 13,14)
8. [Information Security README.md](infosec/README.md "My information security README.md file")
    - This file includes all of the assingments and projects completed during my information security class.
    - Screenshots are provided.