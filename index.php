<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates the skills I've learned over the semester. This portoflio focuses on mobile web app development using native and non-native development. This portfolio also focuses on information security.">
	<meta name="author" content="Kody Horvath">
	<link rel="icon" href="img/favicon.ico">

	<title>My Online Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

<!-- Carousel styles -->
<style type="text/css">
h2
{
	margin: 0;     
	color: #666;
	padding-top: 50px;
	font-size: 52px;
	font-family: "trebuchet ms", sans-serif;    
}
.item
{
	background: #333;    
	text-align: center;
	height: 300px !important;
}
.carousel
{
  margin: 20px 0px 20px 0px;
}
.bs-example
{
  margin: 20px;
}
</style>

</head>
<body>

	<?php include_once("global/nav_global.php"); ?>
	
	<div class="container">
		 <div class="starter-template">
						<div class="page-header">
							<?php include_once("global/header.php"); ?>	
						</div>

<!-- Start Bootstrap Carousel  -->
<div class="bs-example">
	<div
      id="myCarousel"
		class="carousel"
		data-interval="1000"
		data-pause="hover"
		data-wrap="true"
		data-keyboard="true"			
		data-ride="carousel">
		
    	<!-- Carousel indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
       <!-- Carousel items -->
        <div class="carousel-inner">

			<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
          <div class="active item" style="background: url(img/cactus.jpg) no-repeat left center; background-size: cover;">
						<div class="container">
                <div class="carousel-caption">
                  <h3>Aldo</h3>
                  <p class="lead">Meeting with Aldo Bensadoun. Founder and former CEO of Aldo.</p>
						 <a class="btn btn-large btn-primary" href="https://linkedin.com/in/kodyhorvath"> Learn more </a>
                </div>
</div>	

</div>

            <div class="item" style="background: url(img/hydrangea.jpg) no-repeat left center; background-size: cover;">
                
                <div class="carousel-caption">
                <h3>Headshot</h3>
                <p class="lead">My professional headshot.</p>
                <a class="btn btn-large btn-primary" href="https://linkedin.com/in/kodyhorvath">Learn more</a>									
                </div>
            </div>

            <div class="item" style="background: url(img/leaves.jpg) no-repeat left center; background-size: cover;">
            
                <div class="carousel-caption">
                  <h3>Vancouver</h3>
                  <p class="lead">Love to spend time in nature.</p>
				  <a class="btn btn-large btn-primary" href="https://linkedin.com/in/kodyhorvath">Learn more</a>									
                </div>
            </div>

        </div>
        <!-- Carousel nav -->
        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="carousel-control right" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
<!-- End Bootstrap Carousel  -->
						
<?php
include_once "global/footer.php";
?>

	</div> <!-- end starter-template -->
</div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	
</body>
</html>
