> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Kody Horvath

### Assignment 1 Requirements:

*Three Parts:*

    1. Distributed Version Control with Git and Bitbucket
    2. Development installations
    3. Chapter questions (Ch1,2)
    4. Bitbucket repo links a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

#### README.md file should include the following items:

*Screenshot of AMPPS installation [My PHP Installation](http://localhost:8080 "PHP Localhost");
*Screenshot of running java Hello;
*Screenshot of running Android Studio - My First App;
*Git commands w/ short descriptions;


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Creates an empty Git repository
2. git status - Displays paths that have differences between the idnex file and the current HEAD commit, paths that have differences between the working tree and the index file, and paths in the working tree that are not tracked by Git
3. git add - Updates the index using the current content found in the working tree
4. git commit - Stores the current contents of the index in a new commit along with a log message from the user describing the changes
5. git push - Updates remote refs using local refs, while sedning objects necessary to complete the given refs
6. git pull - Incorporates changes from a remote repository into the current branch
7. git fetch - Fetch branches and/or tags from one or more repositories, along with the objects necessary to complete their histories

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/amp.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
