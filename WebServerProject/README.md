
# LIS 4774 - Information Security

## Kody Horvath


#### Assignment Screenshots:

*Lab 01*:

![word/4774.lab01.Horvath.Kody.docx]

*Lab 02*:

![word/4774.lab02.Horvath.Kody.docx]

*Lab 03*:

![word/4774.lab03.Horvath.Kody.docx]


#### Google Docs Link:

*Web Server Vulnerabilities: Attack and Defense*
[https://docs.google.com/document/d/1x2DuJtN8ylRp2Dsjyejpwii3kA-e2MwFYX0rucHXWjo/edit?usp=sharing]
