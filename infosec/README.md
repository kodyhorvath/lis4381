
# LIS 4774 - Information Security

## Kody Horvath


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/amp.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Google Docs Link:

*Web Server Vulnerabilities: Attack and Defense*
[https://docs.google.com/document/d/1x2DuJtN8ylRp2Dsjyejpwii3kA-e2MwFYX0rucHXWjo/edit?usp=sharing]
