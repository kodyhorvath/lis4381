> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Kody Horvath

### Project 2 Requirements:

*Six Parts:*
    1. Turn off client-side validation
    2. Add server side validation
    3. Used regexp to only allow appropriate characters for each control.
    4. Create two PHP pages (edit_petstore & edit_petstore_process)
    5. Allow edit & delete of records
    6. Chapter questions (Ch 13,14)

#### README.md file should include the following items:

*Screenshot of index.php;
*Screenshot of edit_petstore.php;
*Screenshot of edit_petstore_process.php error;
*Screenshot of RSS Feed;




#### Assignment Screenshots:

*Screenshot of index.php*:

![Main Page Screenshot](img/index.png)

*Screenshot of edit_petstore.php*:

![Edit Petstore Screenshot](img/edit_petstore.png)

*Screenshot of edit_petstore_process.php error*:

![Edit Petstore Process Screenshot](img/edit_petstore_process.png)

*Screenshot of RSS Feed*:

![RSS Feed Screenshot](img/rss_feed.png)

*Link to local lis4381 web app *
[P2 Link to local repo](http://localhost/repos/lis4381/ "My Local Repo")