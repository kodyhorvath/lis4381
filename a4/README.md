> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Kody Horvath

### Assignment 4 Requirements:

*Six Parts:*

    1. Clone starter files
    2. Review subdirectories and files
    3. Review index.php code
    4. Research validation code
    5. Create a favicon
    6. Chapter questions (Ch 9,10, 15)

#### README.md file should include the following items:

*Screenshot of LIS4381 Portal;
*Screenshot of failed validation;
*Screenshot of passed validation;




#### Assignment Screenshots:

*Screenshot of LIS4381 Portal (Main Page)*:

![Main Page Screenshot](img/main.png)

*Screenshot of failed validation*:

![Failed Validation Screenshot](img/failed.png)

*Screenshot of passed validation*:

![Passed Validation Screenshot](img/passed.png)

*Link to local lis4381 web app *
[A4 Link to local repo](http://localhost/repos/lis4381/ "My Local Repo")